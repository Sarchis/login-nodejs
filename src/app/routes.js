module.exports = (app, passport) => {

// Index routes
    app.get('/', (req, res) => {
        res.render('index', {
            title: "Inicio"
        });
    });

// Login view
    app.get('/login', (req, res) => {
        res.render('login.ejs', {
            message: req.flash('loginMessage'),
            title: "Login"
        });
    });

    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/profile',
        failureRedirect: '/login',
        failureFlash: true
    }))


// signup view
    app.get('/signup', (req, res) => {
        res.render('signup', {
            message: req.flash('signupMessage '),
            title: "Crea tu cuenta"
        })
    })


    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/profile',
        failureRedirect: '/signup',
        failureFlash: true
    }));


// Profile View
    app.get('/profile', isLoggedIn, (req, res) => {
        res.render('profile', {
            user: req.user
        });
    });


// Logout
    app.get('/logout', (req, res) => {
        req.logout();
        res.redirect('/');
    });
};

function isLoggedIn (req, res, next) {
    if(req.isAuthenticated()){
        return next();
    }
    res.redirect('/');
}
